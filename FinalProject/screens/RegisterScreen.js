import React, { useState } from 'react'
import { StyleSheet, Text, View, TextInput, TouchableOpacity, StatusBar, Alert } from 'react-native'
import firebase from 'firebase';

import getData from '../components/GetData';

const firebaseConfig = {
    apiKey: "AIzaSyClccNkLepPC28nBFtlynWFlECBUF3uhhI",
    authDomain: "finalproject-4cdba.firebaseapp.com",
    databaseURL: "https://finalproject-4cdba-default-rtdb.asia-southeast1.firebasedatabase.app",
    projectId: "finalproject-4cdba",
    storageBucket: "finalproject-4cdba.appspot.com",
    messagingSenderId: "413823638979",
    appId: "1:413823638979:web:07b563f4f7f4649a073176",
    measurementId: "G-T62JYPL8QW"
};

if (firebase.apps.length === 0) {
    firebase.initializeApp(firebaseConfig);
}

var database = firebase.database();

export default function RegisterScreen({navigation}) {
    const [name, setName] = useState("");
    const [email, setEmail] = useState("");
    const [phone, setPhone] = useState("");
    const [password, setPassword] = useState("");
    const [confirmPass, setConfirmPass] = useState("");

    const clearInput = () => {
        setName("");
        setEmail("");
        setPhone("");
        setPassword("");
        setConfirmPass("");
    }

    const autoLogin = () => {
        firebase.auth().signInWithEmailAndPassword(email, password)
        .then((userCredential) => {
            // Signed in
            var user = userCredential.user;
            const UID = firebase.auth().currentUser.uid;
            firebase.database().ref('UserInfo/' + UID).set({
                name: name,
                email: email,
                phone: phone,
                password: password,
                balance: 0
            });
            getData(UID);
            setTimeout(() => {
                getData(UID);
            }, 3000);
            navigation.navigate("HomeScreen", {reload: Math.random()});
            clearInput();
        })
        .catch((error) => {
          var errorCode = error.code;
          var errorMessage = error.message;
          alert(errorMessage);
        });
    }

    const submit = () => {
        if (!name || !email || !phone || !password || !confirmPass) {
            alert("All forms must be filled.")
        }
        else if (confirmPass != password) {
            alert("Your password and confirmation password do not match.")
        }
        else {
            firebase.auth().createUserWithEmailAndPassword(email, password)
            .then((userCredential) => {
                // Signed in 
                var user = userCredential.user;
                Alert.alert(
                    "Congratulations!",
                    "Your account has been registered successfully.",
                    [
                      { text: "OK", onPress: () => {autoLogin()} }
                    ]
                );
            })
            .catch((error) => {
              var errorCode = error.code;
              var errorMessage = error.message;
              alert(errorMessage);
            });
        }
    }

    const login = () => {
        navigation.navigate("LoginScreen")
        clearInput()
    }

    return (
        <View style = {styles.container}>
            <StatusBar backgroundColor = "white" barStyle = "dark-content"/>
            <Text style = {styles.title}>Sign Up</Text>
            <View style = {styles.textContainer}>
                <TextInput
                  style={{ height: 40, paddingLeft: 15 }}
                  onChangeText={text => setName(text)}
                  value={name}
                  placeholder="Name"
                />
            </View>
            <View style = {styles.textContainer}>
                <TextInput
                  style={{ height: 40, paddingLeft: 15 }}
                  onChangeText={text => setEmail(text)}
                  value={email}
                  placeholder="Email"
                />
            </View>
            <View style = {styles.textContainer}>
                <TextInput
                  style={{ height: 40, paddingLeft: 15 }}
                  onChangeText={text => setPhone(text)}
                  value={phone}
                  placeholder="Phone Number"
                  keyboardType="numeric"
                />
            </View>
            <View style = {styles.textContainer}>
                <TextInput
                  style={{ height: 40, paddingLeft: 15 }}
                  onChangeText={text => setPassword(text)}
                  value={password}
                  placeholder="Password"
                  secureTextEntry={true}
                />
            </View>
            <View style = {styles.textContainer}>
                <TextInput
                  style={{ height: 40, paddingLeft: 15 }}
                  onChangeText={text => setConfirmPass(text)}
                  value={confirmPass}
                  placeholder="Confirm Password"
                  secureTextEntry={true}
                />
            </View>
            <TouchableOpacity style = {styles.registerButton} onPress = {submit}>
                <Text style = {styles.textButton}>Register</Text>
            </TouchableOpacity>
            <View style = {{flexDirection: 'row', alignSelf: 'flex-start', marginLeft: 50}}>
                <Text style = {styles.textRegister}>Already registered?</Text>
                <TouchableOpacity onPress = {login}>
                    <Text style = {styles.loginButton}>  Login</Text>
                </TouchableOpacity>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'black',
        alignItems: 'center',
        justifyContent: 'center'
    },
    title: {
        color: 'white',
        fontSize: 36,
        marginBottom: 60
    },
    textContainer: {
        backgroundColor: 'white',
        height: 40,
        width: 300,
        marginTop: 10,
        borderRadius: 3,
        alignSelf: 'center'
    },
    registerButton: {
        width: 300,
        height: 40,
        backgroundColor: '#46FF4E',
        borderRadius: 5,
        marginTop: 40,
        marginVertical: 10,
        alignItems: 'center',
        justifyContent: 'center'
    },
    textButton: {
        fontSize: 14,
        fontWeight: 'bold'
    },
    textRegister: {
        fontSize: 12,
        color: 'white'
    },
    loginButton: {
        fontSize: 12,
        fontWeight: 'bold',
        color: '#46FF4E'
    }
})