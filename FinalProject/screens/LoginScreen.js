import React, {useState} from 'react'
import { StyleSheet, Text, TextInput, TouchableOpacity, View, Image, StatusBar } from 'react-native'
import firebase from 'firebase';
import getData from '../components/GetData';

export default function LoginScreen({navigation}) {
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");

    const clearInput = () => {
        setEmail("");
        setPassword("");
    }

    const submit = () => {
        firebase.auth().signInWithEmailAndPassword(email, password)
        .then((userCredential) => {
            // Signed in
            var user = userCredential.user;
            const UID = firebase.auth().currentUser.uid;
            getData(UID);
            setTimeout(() => {
                getData(UID);
            }, 3000);
            navigation.navigate("HomeScreen", {reload: Math.random()});
            clearInput();
        })
        .catch((error) => {
          var errorCode = error.code;
          var errorMessage = error.message;
          alert(errorMessage);
        });
    }

    const regis = () => {
        navigation.navigate("RegisterScreen")
        clearInput()
    }

    return (
        <View style = {styles.container}>
            <StatusBar backgroundColor = "white" barStyle = "dark-content"/>
            <Image
                style = {styles.logoContainer}
                source = {require('../assets/Logo.png')}
            />
            <Text style = {styles.title}>Sign In</Text>
            <View style = {styles.textContainer}>
                <TextInput
                    style={{ height: 40, paddingLeft: 15 }}
                    onChangeText={text => setEmail(text)}
                    value={email}
                    placeholder="Email"
                />
            </View>
            <View style = {styles.textContainer}>
                <TextInput
                  style={{ height: 40, paddingLeft: 15 }}
                  onChangeText={text => setPassword(text)}
                  value={password}
                  placeholder="Password"
                  secureTextEntry={true}
                />
            </View>
            <TouchableOpacity style = {styles.loginButton} onPress = {submit}>
                <Text style = {styles.textButton}>Sign In</Text>
            </TouchableOpacity>
            <View style = {{flexDirection: 'row'}}>
                <Text style = {styles.textRegister}>Not a member?</Text>
                <TouchableOpacity onPress = {regis}>
                    <Text style = {styles.registerButton}>  Sign Up </Text>
                </TouchableOpacity>
                <Text style = {styles.textRegister}>now.</Text>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'black',
        alignItems: 'center',
        justifyContent: 'center'
    },
    logoContainer: {
        width: 208,
        height: 121,
        marginBottom: 60
    },
    title: {
        color: 'white',
        fontSize: 36,
        alignSelf: 'flex-start',
        marginLeft: 40
    },
    textContainer: {
        backgroundColor: 'white',
        height: 40,
        width: 300,
        marginTop: 10,
        borderRadius: 3,
        alignSelf: 'center'
    },
    loginButton: {
        width: 300,
        height: 40,
        backgroundColor: '#46FF4E',
        borderRadius: 5,
        marginTop: 60,
        marginVertical: 10,
        alignItems: 'center',
        justifyContent: 'center'
    },
    textButton: {
        fontSize: 14,
        fontWeight: 'bold'
    },
    textRegister: {
        fontSize: 12,
        color: 'white'
    },
    registerButton: {
        fontSize: 12,
        fontWeight: 'bold',
        color: '#46FF4E'
    }
})
