import React from 'react'
import { StyleSheet, Image, StatusBar } from 'react-native'

export default function SplashScreen() {
    return (
        <View style = {styles.container}>
            <StatusBar hidden = {true}/>
            <Image
                style = {styles.splash}
                source = {require('../assets/Logo.png')}
            />
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'black',
        alignItems: 'center',
        justifyContent: 'center',
    },
    splash: {
        width: 264,
        height: 153
    }
})
