import React, { useState, useEffect } from 'react'
import { StyleSheet, Text, View, TouchableOpacity, StatusBar, Image, FlatList } from 'react-native'

import { GlobalData } from '../context/GlobalData';
import displayNumber from '../components/DisplayNumber';

export default function ProfileScreen({navigation}) {
    const [buttonPressed, setButtonPressed] = useState(false)

    const detailButton = (value) => {
        navigation.navigate("DetailScreen", {
            product: value
        });
    }

    return (
        <View style = {styles.container}>
            <StatusBar backgroundColor = "white" barStyle = "dark-content"/>
            <View style = {styles.header}>
                <Text style = {styles.headerText}>My Account</Text>
            </View>
            <Text style = {styles.userName}>{GlobalData.name}</Text>
            <View style = {styles.profileContainer}>
                <Image
                    style = {styles.profileImage}
                    source = {require('../assets/dummy_user.png')}
                />
                <View style = {styles.balanceContainer}>
                    <View style = {{flexDirection: 'row'}}>
                        <TouchableOpacity onPress = {() => {navigation.navigate("TopUpScreen")}}>
                            <Text style = {styles.topUpButton}>Top Up    </Text>
                        </TouchableOpacity>
                        <Text style = {styles.balanceText}>Your Balance:</Text>
                    </View>
                    <Text style = {styles.balanceAmount}>{`Rp. ${displayNumber(GlobalData.balance)}`}</Text>
                </View>
            </View>
            <View style = {styles.detailContainer}>
                <View style = {{flexDirection: 'row'}}>
                    <TouchableOpacity onPress = {() => setButtonPressed(false)}>
                        {buttonPressed ? (
                            <View style = {styles.detailButton}>
                                <Text style = {styles.detailButtonText}>Cart</Text>
                            </View>
                        ) : (
                            <View style = {styles.detailButtonPressed}>
                                <Text style = {styles.detailButtonText}>Cart</Text>
                            </View>
                        )}
                    </TouchableOpacity>
                    <TouchableOpacity onPress = {() => setButtonPressed(true)}>
                        {buttonPressed ? (
                            <View style = {styles.detailButtonPressed}>
                                <Text style = {styles.detailButtonText}>User Details</Text>
                            </View>
                        ) : (
                            <View style = {styles.detailButton}>
                                <Text style = {styles.detailButtonText}>User Details</Text>
                            </View>
                        )}
                    </TouchableOpacity>
                </View>
                { buttonPressed ? (
                    <View style = {styles.detailList}>
                        <Text style = {styles.detailText}>{`Name: ${GlobalData.name}`}</Text>
                        <Text style = {styles.detailText}>{`Email: ${GlobalData.email}`}</Text>
                        <Text style = {styles.detailText}>{`Phone: ${GlobalData.phone}`}</Text>
                    </View> 
                ) : (
                    <View style = {styles.cartList}>
                        { GlobalData.cart.length ?
                            (
                                <FlatList
                                    horizontal
                                    showsHorizontalScrollIndicator = {false}
                                    data = {GlobalData.cart}
                                    keyExtractor = {item => item.id}
                                    renderItem = {({item}) => {
                                        return (
                                            <TouchableOpacity onPress = {() => {detailButton(item)}}>
                                                <View style = {styles.productContent}>
                                                    <Image
                                                        style = {styles.productImage}
                                                        source = {item.image}
                                                    />
                                                </View>
                                                <View style = {styles.footerContent}>
                                                    <Text style = {styles.productTitle}>{item.title}</Text>
                                                    <Text style = {styles.productPrice}>Rp. {displayNumber(item.price)}</Text>
                                                </View>
                                            </TouchableOpacity>
                                        )
                                    }}
                                />
                            ) : (
                                <View style = {styles.cartEmpty}>
                                    <Text style = {styles.emptyText}>Your cart is empty.</Text>
                                </View>
                            )
                        }
                    </View>
                )}
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'black',
        alignItems: 'center',
    },
    header: {
        backgroundColor: '#252525',
        width: 360,
        height: 90,
        justifyContent: 'center'
    },
    headerText: {
        color: 'white',
        fontSize: 24,
        alignSelf: 'flex-start',
        marginLeft: 40
    },
    userName: {
        color: 'white',
        fontSize: 18,
        alignSelf: 'flex-start',
        marginLeft: 25,
        marginVertical: 10
    },
    profileContainer: {
        backgroundColor: '#252525',
        width: 360,
        height: 118,
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center'
    },
    profileImage: {
        width: 75,
        height: 75,
    },
    balanceContainer: {
        justifyContent: 'center'
    },
    balanceText: {
        color: 'white',
        fontSize: 14,
    },
    balanceAmount: {
        color: 'white',
        fontSize: 18,
    },
    topUpButton: {
        color: '#46FF4E',
        fontSize: 14,
    },
    detailContainer: {
        backgroundColor: '#252525',
        width: 360,
        height: 184,
        marginTop: 27
    },
    detailButton: {
        width: 180,
        height: 40,
        alignItems: 'center',
        justifyContent: 'center'
    },
    detailButtonPressed: {
        width: 180,
        height: 40,
        borderBottomWidth: 1,
        borderBottomColor: 'white',
        alignItems: 'center',
        justifyContent: 'center'
    },
    detailButtonText: {
        color: 'white',
        fontSize: 14,
    },
    detailList: {
        height: 250,
        backgroundColor: '#252525',
        paddingTop: 30,
        paddingLeft: 20
    },
    detailText: {
        color: 'white',
        fontSize: 12,
        marginVertical: 5,
    },
    cartList: {
        height: 250,
        backgroundColor: '#252525',
        justifyContent: 'center',
        flexDirection: 'row'
    },
    cartText: {
        color: 'white',
        fontSize: 12,
    },
    loading: {
        flex: 1,
        backgroundColor: 'black',
        alignItems: 'center',
        justifyContent: 'center'
    },
    productContent: {
        backgroundColor: '#444444',
        width: 120,
        height: 140,
        alignItems: 'center',
        justifyContent: 'center',
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
        marginHorizontal: 10,
        marginTop: 30
    },
    productImage: {
        width: 100,
        height: 100
    },
    footerContent: {
        backgroundColor: '#AAAAAA',
        width: 120,
        height: 50,
        borderBottomLeftRadius: 10,
        borderBottomRightRadius: 10,
        marginHorizontal: 10,
        paddingLeft: 10,
        justifyContent: 'center'
    },
    productTitle: {
        fontSize: 11,
    },
    productPrice: {
        fontSize: 9
    },
    cartEmpty: {
        justifyContent: 'center'
    },
    emptyText: {
        color: 'white',
        fontSize: 14
    }
})
