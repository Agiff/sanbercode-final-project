import React from 'react'
import { Image, StatusBar, StyleSheet, Text, TouchableOpacity, View } from 'react-native'

export default function WelcomeScreen({navigation}) {
    return (
        <View style = {styles.container}>
            <StatusBar backgroundColor = "white" barStyle = "dark-content"/>
            <Image
                style = {styles.logoContainer}
                source = {require('../assets/Logo.png')}
            />
            <Text style = {styles.title}>Welcome</Text>
            <TouchableOpacity style = {styles.registerButton} onPress = {() => navigation.navigate("RegisterScreen")}>
                <Text style = {styles.textButton}>Register</Text>
            </TouchableOpacity>
            <TouchableOpacity style = {styles.loginButton} onPress = {() => navigation.navigate("LoginScreen")}>
                <Text style = {styles.textButton}>Login</Text>
            </TouchableOpacity>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'black',
        alignItems: 'center',
        justifyContent: 'center'
    },
    logoContainer: {
        width: 208,
        height: 121,
        marginVertical: 20
    },
    title: {
        color: 'white',
        fontSize: 36,
        marginVertical: 80
    },
    registerButton: {
        width: 300,
        height: 40,
        backgroundColor: '#46FF4E',
        borderRadius: 5,
        marginTop: 30,
        marginVertical: 10,
        alignItems: 'center',
        justifyContent: 'center'
    },
    loginButton: {
        width: 300,
        height: 40,
        backgroundColor: 'white',
        borderRadius: 5,
        marginVertical: 10,
        alignItems: 'center',
        justifyContent: 'center'
    },
    textButton: {
        fontSize: 14,
        fontWeight: 'bold'
    }
})
