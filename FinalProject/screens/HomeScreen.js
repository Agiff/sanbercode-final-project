import React, { useState, useEffect } from 'react'
import { StyleSheet, Text, View, TouchableOpacity, StatusBar, Image, FlatList, ScrollView, ActivityIndicator } from 'react-native'

import { Data } from '../components/ProductData';
import { GlobalData } from '../context/GlobalData';
import displayNumber from '../components/DisplayNumber';

export default function HomeScreen({route, navigation}) {
    const { reload } = route.params;
    const [isLoading, seIsLoading] = useState(true)

    const firstName = () => {
        const firstName = GlobalData.name.split(' ')

        return firstName[0]
    }

    const detailButton = (value) => {
        navigation.navigate("DetailScreen", {
            product: value
        });
    }

    useEffect(() => {
        const timeout = setTimeout(() => {
            seIsLoading (false);
        }, 3000);
        
        return () => {
            clearTimeout(timeout);
        };
    }, [reload]);

    return isLoading ? (  
        <View style = {styles.loading}>
            <ActivityIndicator size="large" color="#00ff00"/>
        </View>
    ) : (
        <View style = {styles.container}>
            <StatusBar backgroundColor = "white" barStyle = "dark-content"/>
            <ScrollView>
            <View style = {styles.header}>
                <View>
                    <Text style = {styles.title}>Welcome,</Text>
                    <Text style = {styles.titleName}>{firstName()}</Text>
                </View>
                <TouchableOpacity onPress = {() => navigation.navigate("ProfileScreen")}>
                    <Image
                        style = {styles.profileButton}
                        source = {require('../assets/dummy_user.png')}
                    />
                </TouchableOpacity>
            </View>
            <View style = {styles.balanceContainer}>
                <Text style = {styles.balanceText}>{`Your Balance: Rp. ${displayNumber(GlobalData.balance)}`}</Text>
                <TouchableOpacity onPress = {() => navigation.navigate("TopUpScreen")}>
                    <Text style = {styles.topUpButton}>Top Up</Text>
                </TouchableOpacity>
            </View>
            <View style = {styles.promoBanner}>
                <Image
                    style = {styles.bannerImage}
                    source = {require('../assets/dummy_banner.png')}
                />
            </View>
            <Text style = {styles.saleTitle}>Flash Sale</Text>
            <View style = {styles.flashSaleContainer}>
                <FlatList
                    horizontal
                    showsHorizontalScrollIndicator = {false}
                    data = {Data}
                    keyExtractor = {item => item.id}
                    renderItem = {({item}) => {
                        if (item.type == "Flash" || item.type == "FlashPopular") {
                            return (
                                <TouchableOpacity onPress = {() => {detailButton(item)}}>
                                    <View style = {styles.productContent}>
                                        <Image
                                            style = {styles.productImage}
                                            source = {item.image}
                                        />
                                    </View>
                                    <View style = {styles.footerContent}>
                                        <Text style = {styles.productTitle}>{item.title}</Text>
                                        <Text style = {styles.productPrice}>Rp. {displayNumber(item.price)}</Text>
                                    </View>
                                </TouchableOpacity>
                            )
                        }
                    }}
                />
            </View>
            <Text style = {styles.saleTitle}>Popular Sale</Text>
            <View style = {styles.popularContainer}>
                <FlatList
                    horizontal
                    showsHorizontalScrollIndicator = {false}
                    data = {Data}
                    keyExtractor = {item => item.id}
                    renderItem = {({item}) => {
                        if (item.type == "Popular" || item.type == "FlashPopular") {
                            return (
                                <TouchableOpacity onPress = {() => {detailButton(item)}}>
                                    <View style = {styles.productContent}>
                                        <Image
                                            style = {styles.productImage}
                                            source = {item.image}
                                        />
                                    </View>
                                    <View style = {styles.footerContent}>
                                        <Text style = {styles.productTitle}>{item.title}</Text>
                                        <Text style = {styles.productPrice}>Rp. {displayNumber(item.price)}</Text>
                                    </View>
                                </TouchableOpacity>
                            )
                        }
                    }}
                />
            </View>
            <Text style = {styles.saleTitle}>All</Text>
            <View style = {styles.allContainer}>
                <FlatList
                    horizontal
                    showsHorizontalScrollIndicator = {false}
                    data = {Data}
                    keyExtractor = {item => item.id}
                    renderItem = {({item}) => {
                        return (
                            <TouchableOpacity onPress = {() => {detailButton(item)}}>
                                <View style = {styles.productContent}>
                                    <Image
                                        style = {styles.productImage}
                                        source = {item.image}
                                    />
                                </View>
                                <View style = {styles.footerContent}>
                                    <Text style = {styles.productTitle}>{item.title}</Text>
                                    <Text style = {styles.productPrice}>Rp. {displayNumber(item.price)}</Text>
                                </View>
                            </TouchableOpacity>
                        )
                    }}
                />
            </View>
            </ScrollView>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'black',
        alignItems: 'center',
    },
    header: {
        marginTop: 30,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'
    },
    title: {
        color: 'white',
        fontSize: 20
    },
    titleName: {
        color: 'white',
        fontSize: 16,
        marginTop: 5
    },
    profileButton: {
        width: 50,
        height: 50,
        marginLeft: 120
    },
    balanceContainer: {
        backgroundColor: '#252525',
        width: 360,
        height: 60,
        marginTop: 30,
        borderRadius: 10,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-around'
    },
    balanceText: {
        color: 'white',
        fontSize: 12
    },
    topUpButton: {
        color: '#46FF4E',
        fontSize: 12
    },
    promoBanner: {
        width: 360,
        height: 80,
        backgroundColor: '#252525',
        borderRadius: 10,
        marginTop: 10,
        alignItems: 'center',
        justifyContent: 'center'
    },
    saleTitle: {
        fontSize: 18,
        color: '#46FF4E',
        fontWeight: 'bold',
        alignSelf: 'flex-start',
        marginLeft: 20,
        marginTop: 20,
    },
    flashSaleContainer: {
        marginVertical: 20
    },
    productContent: {
        backgroundColor: '#252525',
        width: 120,
        height: 140,
        alignItems: 'center',
        justifyContent: 'center',
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
        marginHorizontal: 10
    },
    productImage: {
        width: 100,
        height: 100
    },
    footerContent: {
        backgroundColor: '#C4C4C4',
        width: 120,
        height: 50,
        borderBottomLeftRadius: 10,
        borderBottomRightRadius: 10,
        marginHorizontal: 10,
        paddingLeft: 10,
        justifyContent: 'center'
    },
    productTitle: {
        fontSize: 11,
    },
    productPrice: {
        fontSize: 9
    },
    popularContainer: {
        marginVertical: 20
    },
    allContainer: {
        marginVertical: 20
    },
    loading: {
        flex: 1,
        backgroundColor: 'black',
        alignItems: 'center',
        justifyContent: 'center'
    },
    bannerImage: {
        width: 360,
        height: 80
    }
})
