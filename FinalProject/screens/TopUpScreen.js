import React, { useState, useEffect } from 'react'
import { StyleSheet, Text, TextInput, TouchableOpacity, View, ActivityIndicator } from 'react-native'

import firebase from 'firebase'
import { GlobalData } from '../context/GlobalData'
import getData from '../components/GetData'

export default function TopUpScreen({navigation}) {
    const [input, setInput] = useState("")

    const topUp = () => {
        const amount = Number(GlobalData.balance) + Number(input);
        const UID = firebase.auth().currentUser.uid;
        firebase.database().ref('UserInfo/' + UID).update({balance: amount})
        getData(UID);
        setTimeout(() => {
            getData(UID);
        }, 3000);
        navigation.navigate("HomeScreen", {reload: Math.random()});
    }

    return (
        <View style = {styles.container}>
            <Text style = {styles.title}>Top Up</Text>
            <View style = {styles.topUpContainer}>
                <TextInput
                    style={styles.topUpInput}
                    onChangeText={text => setInput(text)}
                    value={input}
                    placeholder="Top Up amount"
                    keyboardType="numeric"
                />
                <TouchableOpacity style = {styles.topUpButton} onPress = {topUp}>
                    <Text style = {styles.topUpText}>Top Up</Text>
                </TouchableOpacity>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'black',
        alignItems: 'center',
    },
    title: {
        color: 'white',
        fontSize: 36,
        marginVertical: 150
    },
    topUpContainer: {
        alignItems: 'center',
        justifyContent: 'center',
    },
    topUpButton: {
        width: 300,
        height: 40,
        backgroundColor: '#46FF4E',
        borderRadius: 5,
        marginVertical: 20,
        alignItems: 'center',
        justifyContent: 'center'
    },
    topUpText: {
        fontSize: 14,
        fontWeight: 'bold'
    },
    topUpInput: {
        width: 300,
        height: 40,
        paddingLeft: 15,
        backgroundColor: 'white',
        borderRadius: 5
    },
    loading: {
        flex: 1,
        backgroundColor: 'black',
        alignItems: 'center',
        justifyContent: 'center'
    }
})
