import React from 'react'
import { StyleSheet, Text, View, TouchableOpacity, StatusBar, Image, Alert } from 'react-native'
import firebase from 'firebase';

import getData from '../components/GetData';
import { GlobalData } from '../context/GlobalData';
import displayNumber from '../components/DisplayNumber';

export default function DetailScreen({route, navigation}) {
    const { product } = route.params;

    const onBuy = () => {
        const balance = Number(GlobalData.balance);
        const productPrice = Number(product.price);

        if (balance < productPrice) {
            Alert.alert(
                "Purchase failed",
                "Sorry, your balance is not sufficient.",
                [
                    {
                        text: "OK",
                        onPress: () => console.log("Cancel Pressed"),
                        style: "cancel"
                    },
                ]
            );
        }
        else {
            const amount = balance - productPrice;
            const UID = firebase.auth().currentUser.uid;
            Alert.alert(
                "Confirmation",
                "Are you sure you want to buy this?",
                [
                    {
                      text: "Cancel",
                      onPress: () => console.log("Cancel Pressed"),
                      style: "cancel"
                    },
                    { text: "Yes", onPress: () => {
                        firebase.database().ref('UserInfo/' + UID).update({balance: amount})
                        getData(UID);
                        setTimeout(() => {
                            getData(UID);
                        }, 3000)

                        for (let i = 0; i < GlobalData.cart.length; i++) {
                            if (GlobalData.cart[i].id == product.id) {
                                GlobalData.cart.splice(i, 1);
                            }
                        }

                        Alert.alert(
                            "Thank you for the purchase!",
                            `You have bought ${product.title} for Rp. ${displayNumber(product.price)}`,
                            [
                                { text: "OK", onPress: () => {
                                    navigation.navigate("HomeScreen", {reload: Math.random()});
                                }}
                            ]
                        );
                    }}
                ]
            );
        }
    }

    const onCart = () => {
        if (!GlobalData.cart.length) {
            GlobalData.cart.push(product)
            alert("You added this product to the cart.");
        }
        else {
            let check = 0;
            for (let i = 0; i < GlobalData.cart.length; i++) {
                if (GlobalData.cart[i].id == product.id) {
                    check = 1;
                }
            }
            if (check == 1)
                alert("This product is already in the cart.");
            else {
                GlobalData.cart.push(product);
                alert("You added this product to the cart.");
            }
        }
    }

    return (
        <View style = {styles.container}>
            <StatusBar backgroundColor = "white" barStyle = "dark-content"/>
            <View style = {styles.productImageContainer}>
                <Image
                    style = {styles.productImage}
                    source = {product.image}
                />
            </View>
            <View style = {styles.productDetailContainer}>
                <View>
                    <Text style = {styles.productName}>{product.title}</Text>
                    <Text style = {styles.productPrice}>Rp. {displayNumber(product.price)}</Text>
                </View>
                <View>
                    <TouchableOpacity>
                        <Image
                            style = {styles.heartIcon}
                            source = {require('../assets/heart.png')}
                        />
                    </TouchableOpacity>
                </View>
            </View>
            <View style = {styles.footerProductDetail}>
                <View>
                    <Image
                        style = {styles.starIcon}
                        source = {require('../assets/star.png')}
                    />
                </View>
                <Text style = {styles.footerText}>{product.star}</Text>
                <Text style = {styles.footerSmallText}>/5</Text>
                <Text style = {styles.footerText}> | </Text>
                <Text style = {styles.footerText}>{product.saleRate} Sold</Text>
                <Text style = {styles.footerSmallText}> /day</Text>
                <View>
                    <Image
                        style = {styles.shippingIcon}
                        source = {require('../assets/shipping.png')}
                    />
                </View>
                <Text style = {styles.footerText}>Free delivery</Text>
            </View>
            <View style = {styles.productDescContainer}>
                <Text style = {styles.productDesc}>{product.desc}</Text>
            </View>
            <View style = {styles.footer}>
                <TouchableOpacity>
                    <Image
                        style = {styles.chatIcon}
                        source = {require('../assets/chat.png')}
                    />
                </TouchableOpacity>
                <TouchableOpacity style = {styles.buyNowButton} onPress = {() => {onBuy()}}>
                    <Text style = {styles.buttonText}>Buy Now</Text>
                </TouchableOpacity>
                <TouchableOpacity style = {styles.AddToCartButton} onPress = {() => {onCart()}}>
                    <Image
                        style = {styles.cartIcon}
                        source = {require('../assets/cart.png')}
                    />
                    <Text style = {styles.buttonText}>Add to Cart</Text>
                </TouchableOpacity>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'black',
        alignItems: 'center'
    },
    productImageContainer: {
        width: 360,
        height: 320,
        backgroundColor: '#252525',
        alignItems: 'center',
        justifyContent: 'center'
    },
    productImage: {
        width: 240,
        height: 240
    },
    productDetailContainer: {
        width: 360,
        height: 90,
        backgroundColor: '#323232',
        alignItems: 'center',
        justifyContent: 'space-around',
        flexDirection: 'row',
    },
    productName: {
        color: 'white',
        fontSize: 18
    },
    productPrice: {
        color: 'white',
        fontSize: 14
    },
    heartIcon: {
        width: 40,
        height: 40,
        marginLeft: 100
    },
    footerProductDetail: {
        width: 360,
        height: 50,
        backgroundColor: '#323232',
        marginTop: 2,
        borderBottomLeftRadius: 10,
        borderBottomRightRadius: 10,
        flexDirection: 'row',
        alignItems: 'center'
    },
    starIcon: {
        width: 20,
        height: 20,
        marginLeft: 20,
        marginRight: 5
    },
    footerText: {
        color: 'white',
        fontSize: 12
    },
    footerSmallText: {
        color: 'white',
        fontSize: 8
    },
    shippingIcon: {
        width: 35,
        height: 20,
        marginLeft: 40,
        marginRight: 5
    },
    footer: {
        width: 360,
        height: 75,
        backgroundColor: '#252525',
        borderTopRightRadius: 10,
        borderTopLeftRadius: 10,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-around',
        position: 'absolute',
        left: 0,
        right: 0,
        bottom: 0
    },
    chatIcon: {
        width: 40,
        height: 41
    },
    buyNowButton: {
        width: 140,
        height: 41,
        backgroundColor: 'white',
        borderRadius: 5,
        alignItems: 'center',
        justifyContent: 'center'
    },
    buttonText: {
        fontSize: 12,
        fontWeight: 'bold'
    },
    AddToCartButton: {
        width: 140,
        height: 41,
        backgroundColor: '#46FF4E',
        borderRadius: 5,
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row'
    },
    cartIcon: {
        width: 25,
        height: 25,
        marginRight: 5
    },
    productDescContainer: {
        alignSelf: 'flex-start',
        padding: 10
    },
    productDesc: {
        fontSize: 12,
        color: 'white'
    }
})
