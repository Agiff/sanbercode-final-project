import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { createStackNavigator } from '@react-navigation/stack'
import { NavigationContainer, NavigatorContainer } from '@react-navigation/native';

import WelcomeScreen from '../screens/WelcomeScreen'
import LoginScreen from '../screens/LoginScreen'
import RegisterScreen from '../screens/RegisterScreen'
import HomeScreen from '../screens/HomeScreen';
import DetailScreen from '../screens/DetailScreen';
import ProfileScreen from '../screens/ProfileScreen';
import TopUpScreen from '../screens/TopUpScreen';

const Stack = createStackNavigator();

export default function index() {
    return (
        <NavigationContainer>
            <Stack.Navigator screenOptions = {{headerShown: false}}>
                <Stack.Screen name = "WelcomeScreen" component = {WelcomeScreen}/>
                <Stack.Screen name = "LoginScreen" component = {LoginScreen}/>
                <Stack.Screen name = "RegisterScreen" component = {RegisterScreen}/>
                <Stack.Screen name = "HomeScreen" component = {HomeScreen}/>
                <Stack.Screen name = "DetailScreen" component = {DetailScreen}/>
                <Stack.Screen name = "ProfileScreen" component = {ProfileScreen}/>
                <Stack.Screen name = "TopUpScreen" component = {TopUpScreen}/>
            </Stack.Navigator>
        </NavigationContainer>
    )
}

const styles = StyleSheet.create({})
