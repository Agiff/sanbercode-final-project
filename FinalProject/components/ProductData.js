const Data =[
    {
        id: "1",
        price: "450000",
        image: require("../assets/1.png"),
        title: "9Lives",
        desc: "9Lives, makanan kucing 20kg sehat bergizi. Dijamin lincah!",
        star: "4.3",
        saleRate: "52",
        type: "Flash"
    },
    {
        id: "2",
        price: "380000",
        image: require("../assets/2.png"),
        title: "Kit Cat (Green)",
        desc: "Kit Cat Fillet 'o' Flakes, makanan kucing 20kg sehat bergizi. Enak!",
        star: "4.6",
        saleRate: "124",
        type: "Popular"
    },
    {
        id: "3",
        price: "380000",
        image: require("../assets/3.png"),
        title: "Kit Cat (Pink)",
        desc: "Kit Cat Mini Fish Medley, makanan kucing 20kg sehat bergizi. Kuat!",
        star: "4.7",
        saleRate: "132",
        type: "Popular"
    },
    {
        id: "4",
        price: "550000",
        image: require("../assets/4.png"),
        title: "Max's",
        desc: "Max's, makanan kucing 20kg sehat bergizi. Pokoknya max!",
        star: "4.9",
        saleRate: "186",
        type: "FlashPopular"
    },
    {
        id: "5",
        price: "400000",
        image: require("../assets/5.png"),
        title: "Hills",
        desc: "Hills, makanan kucing 20kg sehat bergizi. Untuk kucing diet.",
        star: "4.5",
        saleRate: "172",
        type: "Popular"
    },
    {
        id: "6",
        price: "530000",
        image: require("../assets/6.png"),
        title: "ProPlan",
        desc: "ProPlan, makanan kucing 20kg sehat bergizi. Untuk pro player.",
        star: "4.8",
        saleRate: "85",
        type: "Flash"
    },
    {
        id: "7",
        price: "460000",
        image: require("../assets/7.png"),
        title: "Lara",
        desc: "Lara, makanan kucing 20kg sehat bergizi. Bagus!",
        star: "4.2",
        saleRate: "63",
        type: ""
    },
    {
        id: "8",
        price: "440000",
        image: require("../assets/8.png"),
        title: "Instinct",
        desc: "Instinct, makanan kucing 20kg sehat bergizi. Mempertajam insting.",
        star: "4.3",
        saleRate: "87",
        type: ""
    },
    {
        id: "9",
        price: "420000",
        image: require("../assets/9.png"),
        title: "Royal Canin",
        desc: "Royal Canin, makanan kucing 20kg sehat bergizi. Makanan royal.",
        star: "4.6",
        saleRate: "189",
        type: "Popular"
    },
    {
        id: "10",
        price: "480000",
        image: require("../assets/10.png"),
        title: "Whiskas",
        desc: "Whiskas, makanan kucing 20kg sehat bergizi. Makanan mainstream.",
        star: "4.8",
        saleRate: "271",
        type: "FlashPopular"
    },
]

export {Data};