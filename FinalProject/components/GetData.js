import firebase from 'firebase';

import { GlobalData } from '../context/GlobalData';

const getData = (val1) => {
    let data = "";
    const sourceName = firebase.database().ref().child(`UserInfo/${val1}`);
    
    sourceName.on('value', (snapshot) => {
        data = snapshot.val();
        // console.log(data);
    })

    GlobalData.name = data.name
    GlobalData.email = data.email
    GlobalData.phone = data.phone
    GlobalData.balance = data.balance
};

export default getData