const displayNumber = (value) => {
    var result = value.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1.");

    return result
}

export default displayNumber