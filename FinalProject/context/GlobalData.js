const GlobalData = {
    name: "",
    email: "",
    phone: "",
    balance: 0,
    cart: []
};

export { GlobalData };